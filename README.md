# java-cucumber-serenity-restassured
This is a Rest Assured BDD framework for API test automation. Used serenity + cucumber to  implement BDD approach for api automation

This framework can be made reusable/scalable by building custom base library and helper methods for better reusability.
# Run Instructions:

Run it as 'mvn clean verify' from the project dir

# Report Dir:
/target/site/reports

# Automation Strategy /development

Add scenarios in feature file
generate step definition
write utility code
improve reporting
integrate with ci/cd
