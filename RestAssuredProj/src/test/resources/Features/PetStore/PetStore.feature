Feature: Pet Store

	I upload the pet store image by giving pet id and metadata

	Scenario Outline: Upload petstore image
    Given I provide petid "<petId>" and additional data "<metaData>"
    When I send request to petstore
    Then I validate the response
 
  Examples: Invalid
  			|petId|metaData|
		  	|223344|abc122|
  	
    
    