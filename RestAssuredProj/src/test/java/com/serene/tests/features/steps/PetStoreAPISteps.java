package com.serene.tests.features.steps;

import org.junit.Assert;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import net.thucydides.core.annotations.Step;


public class PetStoreAPISteps {
	
    private Response res = null; //Response
    private JsonPath jp = null; //JsonPath
    private RequestSpecification requestSpec;
	
        @Step
        public void givenPetDetails (int petId,String metaData){
        	RequestSpecBuilder builder = new RequestSpecBuilder();
        	builder.setBasePath(petId+"/"+"uploadImage");
        	builder.setContentType("application/json");
        	System.out.println("{\"petId\":\""+petId+"\",\"metadata\": \""+metaData+"\"}");
        	builder.setBody("{\"petId\":\""+petId+"\",\"additionalMetadata\": \""+metaData+"\"}");
        	requestSpec = builder.build();
        	requestSpec = RestAssured.given().spec(requestSpec);
        	requestSpec.log().all();

        }
        
        @Step     
        public void postPetStoreRequest()  {
        	res = requestSpec.when().post();
          }

        @Step     
        public void verifyPetStoreImageUploadSuccess(int petId,String metaData) {
        	jp = res.jsonPath();
            Assert.assertEquals("Status Check", 200, res.getStatusCode());
        }
   }


