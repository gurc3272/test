package com.serene.tests.features.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Steps;

import org.junit.runner.RunWith;



@RunWith(SerenityRunner.class)


public class PetStoreStepDefn {
	

	@Before
	public void setup()
	{
    	RestAssured.baseURI = "https://petstore.swagger.io/v2/pet/";

	}
	
	@After
	public void tearDown()
	{
        RestAssured.reset();
	}
	@Steps
	com.serene.tests.features.steps.PetStoreAPISteps petStoreAPI;

	@Given("^I provide petid \"([^\"]*)\" and additional data \"([^\"]*)\"$")
	public void i_provide_petid_and_additional_data(int petId, String metaData) throws Exception {
		Serenity.setSessionVariable("petId").to(petId);
		Serenity.setSessionVariable("additionalMetadata").to(metaData);
		petStoreAPI.givenPetDetails(petId,metaData);
	}

	@When("^I send request to petstore$")
	public void i_send_request_to_petstore() throws Exception  {
		petStoreAPI.postPetStoreRequest();
	}

	@Then("^I validate the response$")
	public void i_validate_the_response() throws Exception {
		int petId = Serenity.sessionVariableCalled("petId");
		String metadata = Serenity.sessionVariableCalled("additionalMetadata").toString();
		petStoreAPI.verifyPetStoreImageUploadSuccess(petId,metadata);
		}
}
